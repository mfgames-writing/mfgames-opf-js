export class OpfManifest
{
    constructor(id: string, href: string, mime: string)
    {
        this.id = id;
        this.href = href;
        this.mime = mime;
    }

    public id: string;
    public href: string;
    public mime: string;

    public write(xml: any)
    {
        xml.ele("opf:item", {
            "id": this.id,
            "href": this.href,
            "media-type": this.mime
        });
    }
}
