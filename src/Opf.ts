const xmlBuilder = require("xmlbuilder");
import { OpfCreator } from "./OpfCreator";
import { OpfGuide } from "./OpfGuide";
import { OpfGuideCollection } from "./OpfGuideCollection";
import { OpfManifest } from "./OpfManifest";
import { OpfSpine } from "./OpfSpine";
import { OpfDate } from "./OpfDate";
import { OpfIdentifier } from "./OpfIdentifier";
import { OpfPublisher } from "./OpfPublisher";
import { OpfSeries } from "./OpfSeries";

/**
* Defines the top-level class for representing an OPF file.
*/
export class Opf
{
    public contributors: OpfCreator[] = [];
    public coverImage: string;
    public creators: OpfCreator[] = [];
    public dates: OpfDate[] = [];
    public description: string | undefined;
    public guides: OpfGuideCollection = new OpfGuideCollection();
    public identifiers: OpfIdentifier[] = [];
    public language: string;
    public manifests: OpfManifest[] = [];
    public publishers: OpfPublisher[] = [];
    public rights: string | undefined;
    public series: OpfSeries | undefined;
    public spines: OpfSpine[] = [];
    public subjects: string[] = [];
    public title: string;
    public toc: string = "ncx";

    public get uniqueIdentifier(): OpfIdentifier
    {
        return this.identifiers.filter(i => i.unique)[0];
    }

    public toBuffer(): Buffer
    {
        // Write out the prolog for the file.
        let xmlns = "http://www.idpf.org/2007/opf";
        let dcXmlns = "http://purl.org/dc/elements/1.1/";
        let xml = xmlBuilder
            .begin()
            .dec("1.0", "UTF-8", true)
            .ele("opf:package", {
                "version": "2.0",
                "xmlns:opf": xmlns,
                "xmlns:dc": dcXmlns,
                "unique-identifier": this.uniqueIdentifier.type,
            });

        // Write out the metdata.
        this.writeMetadata(xml);
        this.writeManifest(xml);
        this.writeSpine(xml);
        this.writeGuide(xml);

        // Finish up the XML and write it out. This is not a string, so we
        // force it into one by appending "".
        xml.end({ pretty: true });
        return new Buffer(xml + "", "utf-8");
    }

    /**
     * Validates the correctness of the OPF settings.
     *
     * @returns {string[]} A list of validation errors or empty list if
     *  there are no validation errors.
     */
    public validate(): string[]
    {
        // Keep track of the messages.
        let messages: string[] = [];

        // Make sure we have exactly one unique identifier.
        const uniqueIdentifiers = this.identifiers.filter(i => i.unique);

        if (uniqueIdentifiers.length !== 1)
        {
            messages.push("There must exactly one identifier with unique set to true")
        }

        // Return the messages.
        return messages;
    }

    private writeMetadata(xml: any): void
    {
        xml = xml.ele("opf:metadata");

        for (var identifier of this.identifiers)
        {
            identifier.write(xml);
        }

        xml.ele("dc:title", this.title);
        xml.ele("dc:language", this.language);

        for (var creator of this.creators)
        {
            creator.write(xml, "creator");
        }

        for (var creator of this.contributors)
        {
            creator.write(xml, "contributor");
        }

        for (var publisher of this.publishers)
        {
            publisher.write(xml);
        }

        for (var date of this.dates)
        {
            date.write(xml);
        }

        for (var subject of this.subjects)
        {
            xml.ele("dc:subject", {}, subject);
        }

        if (this.rights)
        {
            xml.ele("dc:rights", {}, this.rights);
        }

        if (this.description)
        {
            xml.ele("dc:description", {}, this.description);
        }

        if (this.coverImage)
        {
            xml.ele("meta", { name: "cover", content: this.coverImage });
        }

        if (this.series)
        {
            this.series.write(xml);
        }
    }

    private writeGuide(xml: any): void
    {
        // If we don't have a guide, then don't write it out.
        if (this.guides)
        {
            this.guides.write(xml);
        }
    }

    private writeManifest(xml: any): void
    {
        // Create the top-level manifest element.
        xml = xml.ele("opf:manifest");

        // Add each manifest item into the list.
        for (var manifest of this.manifests)
        {
            manifest.write(xml);
        }
    }

    private writeSpine(xml: any): void
    {
        // Create the top-level manifest element.
        xml = xml.ele("opf:spine", { toc: this.toc });

        // Add each manifest item into the list.
        for (var spine of this.spines)
        {
            spine.write(xml);
        }
    }
}
