export class OpfSpine
{
    constructor(id: string)
    {
        this.id = id;
    }

    public id: string;
    public linear: boolean = true;

    public write(xml: any)
    {
        let attrs: any = {
            "idref": this.id
        };

        if (!this.linear)
        {
            attrs["linear"] = "no";
        }

        xml.ele("opf:itemref", attrs);
    }
}
