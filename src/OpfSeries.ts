export class OpfSeries
{
    name: string | undefined;
    volume: number | undefined;

    constructor(
        name: string | undefined = undefined,
        volume: number | undefined = undefined)
    {
        this.name = name;
        this.volume = volume;
    }

    public write(xml: any)
    {
        if (this.name)
        {
            xml.ele("meta", { content: this.name, name: "calibre:series" });
        }

        if (this.volume)
        {
            xml.ele("meta", { content: this.volume, name: "calibre:series_index" });
        }
    }
}
