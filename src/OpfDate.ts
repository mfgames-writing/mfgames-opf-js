import * as moment from "moment";

export class OpfDate
{
    public date: moment.Moment;

    constructor(date: moment.Moment)
    {
        this.date = date;
    }

    public write(xml: any)
    {
        let attrs: any = {};

        xml.ele("dc:date", attrs, this.date.format("YYYY-MM-DD"));
    }
}
