export class OpfIdentifier
{
    public unique: boolean;
    public type: string;
    public scheme: string;
    public id: string;

    constructor(
        id: string,
        type: string = "book-id",
        scheme: string = "uuid",
        unique: boolean = true)
    {
        this.type = type;
        this.scheme = scheme;
        this.id = id;
        this.unique = unique;
    }

    public write(xml: any)
    {
        let attrs: any = {};

        if (this.type)
        {
            attrs["id"] = this.type;
        }

        if (this.scheme)
        {
            attrs["opf:scheme"] = this.scheme;
        }

        xml.ele("dc:identifier", attrs, this.id);
    }
}
