export class OpfGuide
{
    public href: string;
    public title: string;

    constructor(title: string, href: string)
    {
        this.href = href;
        this.title = title;
    }

    public write(xml: any, type: string)
    {
        xml.ele("opf:reference", {
            "type": type,
            "href": this.href,
            "title": this.title
        });
    }
}
