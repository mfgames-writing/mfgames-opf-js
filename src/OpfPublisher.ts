export class OpfPublisher
{
    name: string;

    constructor(name: string)
    {
        this.name = name;
    }

    public write(xml: any)
    {
        let attrs: any = {};

        xml.ele("dc:publisher", attrs, this.name);
    }
}
