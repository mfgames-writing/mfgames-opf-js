import * as expect from "expect";
import "mocha";
import { OpfIdentifier } from "../OpfIdentifier";
import * as opf from "../index";

describe("guides", function()
{
    it("missing unique identifier", function(callback)
    {
        const t = new opf.Opf();
        const m = t.validate();

        expect(m.length).toBe(1);
        callback();
    });

    it("too many unique identifiers", function(callback)
    {
        const t = new opf.Opf();
        t.identifiers.push(new OpfIdentifier("i", "UUID", "book-id", true));
        t.identifiers.push(new OpfIdentifier("i", "UUID", "book-id2", true));
        const m = t.validate();

        expect(m.length).toBe(1);
        callback();
    });

    it("one unique identifiers", function(callback)
    {
        const t = new opf.Opf();
        t.identifiers.push(new OpfIdentifier("i", "UUID", "book-id", true));
        const m = t.validate();

        expect(m.length).toBe(0);
        callback();
    });
});
