import "mocha";
import * as opf from "../index";
import { OpfGuide } from "../index";

describe("opf", function()
{
    it("add cover once", function(callback)
    {
        const t = new opf.Opf();
        t.guides.cover = new OpfGuide("Cover", "image.jpg");
        callback();
    });
});
