import { OpfGuide } from "./OpfGuide";

/**
 * Container for the unique forms of guides the OPF can handle.
 */
export class OpfGuideCollection
{
    public acknowledgements: OpfGuide | undefined;
    public colophon: OpfGuide | undefined;
    public cover: OpfGuide | undefined;
    public dedication: OpfGuide | undefined;
    public text: OpfGuide | undefined;
    public toc: OpfGuide | undefined;

    public write(xml: any)
    {
        if (!this.cover && !this.toc && !this.text)
        {
            return;
        }

        xml = xml.ele("opf:guide");

        if (this.acknowledgements)
        {
            this.acknowledgements.write(xml, "acknowledgements");
        }

        if (this.colophon)
        {
            this.colophon.write(xml, "colophon");
        }

        if (this.cover)
        {
            this.cover.write(xml, "cover");
        }

        if (this.dedication)
        {
            this.dedication.write(xml, "dedication");
        }

        if (this.text)
        {
            this.text.write(xml, "text");
            this.text.write(xml, "start");
        }

        if (this.toc)
        {
            this.toc.write(xml, "toc");
        }
    }
}
