export class OpfCreator
{
    public name: string;
    public role: string | undefined;

    constructor(name: string, role: string | undefined = undefined)
    {
        this.name = name;
        this.role = role;
    }

    public write(xml: any, type: string)
    {
        let attrs: any = {};

        if (this.role)
        {
            attrs["opf:role"] = this.role;
        }

        xml.ele("dc:" + type, attrs, this.name);
    }
}
