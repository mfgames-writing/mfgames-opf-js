# OPF

`mfgames-opf` is a library for reading, manipulating, and writing out OPF (Open Packaging Format) files used with EPUB files.
