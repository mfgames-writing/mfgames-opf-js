## [0.2.3](https://gitlab.com/mfgames-writing/mfgames-opf-js/compare/v0.2.2...v0.2.3) (2018-08-11)


### Bug Fixes

* do not include tests in index.d.ts ([27868ef](https://gitlab.com/mfgames-writing/mfgames-opf-js/commit/27868ef))

## [0.2.2](https://gitlab.com/mfgames-writing/mfgames-opf-js/compare/v0.2.1...v0.2.2) (2018-08-11)


### Bug Fixes

* added package management ([92baa46](https://gitlab.com/mfgames-writing/mfgames-opf-js/commit/92baa46))
